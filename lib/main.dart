import 'package:flutter/material.dart';
import './ui/welcome_text.dart';

void main() {
  runApp(new MaterialApp(
    title: 'Welcome App',
    home: new Welcome(),
  ));
}
