import 'package:flutter/material.dart';

class Welcome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Material(
        child: new Center(
      child: new Text(
        'Hai Rahul, Its Material',
        textDirection: TextDirection.ltr,
        style: new TextStyle(
            fontWeight: FontWeight.w700,
            fontStyle: FontStyle.italic,
            fontSize: 33.5),
      ),
    ));
  }
}
